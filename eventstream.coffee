EventEmitter = require('eventemitter3')
WebSocket = require('ws')

class EventStream extends EventEmitter
  constructor: (options) ->
    self = this
    @ws = new WebSocket(options.endpoint)
    @ws.on 'open', () ->
      console.log("event streaming started")
    @ws.on 'err', (err) ->
      console.log("error: %s", err)
    @ws.on 'message', (e) ->
      console.log("event: %s", e)
      e = if typeof e == "string" then JSON.parse(e) else e
      if e.name == "message"
        self.emit e.name, e.body

module.exports = EventStream
