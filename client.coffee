request = require('request')
EventEmitter = require('eventemitter3')
EventStream = require('./eventstream')

identity = (t) -> t
trim_slash = (s) ->
  if s.charAt(s.length - 1) == "/" then s.substr(0, s.length - 1) else s

join = () ->
  args = [].slice.call(arguments)
  args.filter(identity).map(trim_slash).join("/")

class HubClient extends EventEmitter
  constructor: (options) ->
    self = this
    if options.endpoint? && options.login? and options.password?
      @endpoint = options.endpoint
      @login = options.login
      @password = options.password
      payload = {
        user: @login
        password: @password
      }
      @post "login", payload, (err, res, body) ->
        throw new Error("Can not login with given credentials") if err
        self.emit "connected"
        self.listen()
    else
      throw new Error("Not enough parameters provided. I need endpoint, login and password")

  listen: () ->
    self = this
    endpoint = "ws" + this.endpoint.substr(4)
    options =
      endpoint: join endpoint, "api/stream/user"
      login: @login
      password: @password
    @events = new EventStream options
    @events.on "message", (msg) ->
      self.emit "message", msg

  send: (room, body, type) ->
    console.log "send message to #{room} with text #{body}"
    payload = 
      body: body
      type: type || "text"
    @post "api/rooms/" + room, payload

  reply: (msg_id, body, type) ->
    console.log "reply to message #{msg_id} with text #{body}"
    payload = 
      body: body
      type: type || "text"
    @post "api/messages/" + msg_id + "/reply", payload

  close: () ->
    # close connections

  post: (path, payload, callback) ->
    unless callback
      callback = (err, res, body) ->
        if err
          console.log("error: %s", err)
    # make request
    opts = {
      method: "POST"
      url: join(@endpoint, path)
      auth: {
        user: @login
        pass: @password
      },
      json: true
      body: payload
    }
    request opts, callback

module.exports = HubClient
