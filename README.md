# Hubot Hub Adapter

## Description

This is the adapter for hubot that allows you to send messages to your Hubot and he will reply back with the response.

## Installation

* Add `hubot-adapter` as a dependency in your hubot's `package.json`

## Usage

You will need to set some environment variables to use this adapter.

    % export HUBOT_LOGIN="hubot"
    % export HUBOT_PASSWORD="hub0t@2015"
