# todo listen message events and if message contains @hubot mention invoke callback to run the robot

# Hubot dependencies
try
  {Robot,Adapter,TextMessage} = require 'hubot'
catch
  console.log "try use parent-require"
  prequire = require('parent-require')
  {Robot,Adapter,TextMessage} = prequire 'hubot'

HubClient = require './client'

class HubAdapter extends Adapter

  error: (err) ->
    console.error err

  send: (envelope, strings...) ->
    console.log "sending"
    content = strings.join "\n"
    @hub.send envelope.room, content

  reply: (envelope, strings...) ->
    console.log "replying"
    content = strings.join "\n"
    @hub.reply(envelope.msg_id, content)

  run: ->
    self = @
    @name = @robot.name || "hubot"

    options =
      endpoint: process.env.HUBOT_ENDPOINT || "http://localhost:3000/"
      login : @name
      password : process.env.HUBOT_PASSWORD || "hub0t2015"

    hub = new HubClient(options)
    hub.on "connected", (e) -> self.handleConnected()
    hub.on "message", (e) -> self.handleMessage(e)
    @hub = hub

  handleConnected: (e) ->
    @emit "connected"

  handleMessage: (msg) ->
    # ignore messages from hubot itself
    user = msg.user_id
    # todo get my user id
    return if user == @name

    # ignore empty bodies
    body = msg.body
    return unless body

    room = msg.room_id || msg.room
    return unless (msg.mentions || []).indexOf(@name) >= 0

    reg = new RegExp('@'+@name,'i')
    text = (body.replace reg, @name).trim()
    console.log "received `#{text}` from #{user}"
    console.log "number of listeners #{@robot.listeners.length}"

    user = @getUser msg, user, room
    tm = new TextMessage user, text, msg.id
    @receive tm
    console.log "handled message #{msg.id} in room #{room}"

  getUser: (msg, name, room) ->
    options =
      login: name
      name: name
      room: room
      msg: msg
      msg_id: msg.id
    return @robot.brain.userForId name, options

module.exports.use = (robot) ->
  new HubAdapter robot
